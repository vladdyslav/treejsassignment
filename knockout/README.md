# README #

* JS Assignment (Knockout Approach)

### About ###
Current project is a **Knockout** approach solving assignment.

Current project implements trees using both Recursive and Iterative methods.
Iterative method is using **NestedSets** to demonstrate implementation.

More about NestedSets could be read here:

* https://en.wikipedia.org/wiki/Nested_set_model
* http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/

### How do I get set up? ###

* Prerequisites
    * You have 'nodejs' to be installed
    * You have 'babel' npm module to be installed (**'npm install babel -g'**)

* How to run tests
    * You can do tests by running  **'npm run test-single-run'** from **current** folder.

* Deployment instructions
    * run **'npm install'**
    * For knockout ES6 is used so first recompile the sources just in case **'npm run build'** (you may have to install babel compiler first by **'npm install babel -g'**)
    * start the application **'npm start'**
    * point your browser to **'localhost:8000'**

### Feedback / Contacts ###
* You can contact me on **vladdyslav-at-gmail-dot-com**