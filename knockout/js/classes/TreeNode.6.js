/*!
 * Tree node class
 * Describes base tree node.
 */

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TreeNode = (function (_CoreModel) {
  _inherits(TreeNode, _CoreModel);

  function TreeNode() {
    _classCallCheck(this, TreeNode);

    _get(Object.getPrototypeOf(TreeNode.prototype), "constructor", this).call(this);

    /**
     * Indicates class type to be instantiated while object is loaded from 
     * model object
     * @property {string} _class
     */
    this.prop("_class", "TreeNode");

    /**
     * Holds id of the node
     * @property {int} id 
     */
    this.prop("id", 0);

    /**
     * Holds parent id of the node
     * Note! Not used for IterativeTreeNodes
     * @property {int} parentId 
     */
    this.prop("parentId", null);

    /**
     * Name of the node displayed to the user
     * @property {string} name Node name
     */
    this.bprop("name", "");

    /**
     * Node collapsed state
     * @property {bool} collapsed
     */
    this.bprop("collapsed", false);

    /**
     * Node editing state
     * @property {bool} edit
     */
    this.edit = ko.observable(false);

    /**
     * Storage for user temporary entered node name
     * @property {string} editname Holds temporary user-entered node name (has to be saved)
     */
    this.editname = ko.observable("");
  }

  /**
   * Disposal (if required)
   */

  _createClass(TreeNode, [{
    key: "dispose",
    value: function dispose() {
      _get(Object.getPrototypeOf(TreeNode.prototype), "dispose", this).call(this);
    }
  }]);

  return TreeNode;
})(CoreModel);
