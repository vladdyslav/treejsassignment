/*!
 * Recursive Tree node class
 * Describes a tree node object for recursive tree.
 */

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RecursiveTreeNode = (function (_TreeNode) {
  _inherits(RecursiveTreeNode, _TreeNode);

  function RecursiveTreeNode() {
    _classCallCheck(this, RecursiveTreeNode);

    _get(Object.getPrototypeOf(RecursiveTreeNode.prototype), "constructor", this).call(this);

    /**
     * Indicates class type to be instantiated while object is loaded from 
     * model object
     * @property {string} _class
     */
    this.prop("_class", "RecursiveTreeNode");

    /**
     * Child nodes storage
     * @property {Array} nodes Array of RecursiveTreeNode
     */
    this.bprop("nodes", []);
  }

  /**
   * Indicates whether node has child nodes
   * @returns {bool}
   */

  _createClass(RecursiveTreeNode, [{
    key: "hasChildren",
    value: function hasChildren() {
      return this.nodes().length > 0;
    }

    /**
     * Indicates whether node has parent node
     * @returns {bool}
     */
  }, {
    key: "hasParent",
    value: function hasParent() {
      return this.parentId !== null;
    }

    /**
     * Disposal (if required)
     */
  }, {
    key: "dispose",
    value: function dispose() {
      _get(Object.getPrototypeOf(RecursiveTreeNode.prototype), "dispose", this).call(this);
    }
  }]);

  return RecursiveTreeNode;
})(TreeNode);
