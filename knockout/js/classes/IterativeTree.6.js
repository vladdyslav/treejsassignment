/**
 * Iterative Tree class
 * Class, to handle and manage tree in an iterative manner.
 * Current tree approach is a JS realization of Nested Sets method.
 * 
 * More information about the method could be found as follows:
 *      https://en.wikipedia.org/wiki/Nested_set_model
 *      http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
 *      
 * Iterative tree nodes are instances of IterativeTreeNode type.
 * Class handles saving and loading data to localStorage under 'IterativeTree' key
 */

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IterativeTree = (function (_CoreModel) {
    _inherits(IterativeTree, _CoreModel);

    /**
     * @constructor
     */

    function IterativeTree() {
        _classCallCheck(this, IterativeTree);

        _get(Object.getPrototypeOf(IterativeTree.prototype), "constructor", this).call(this);

        /**
         * Indicates class type to be instantiated while object is loaded from 
         * model object
         * @property {string} _class
         */
        this.prop("_class", "IterativeTree");

        /**
         * Counter, used for basic unique id generation for nodes
         * @property {int} maxid
         */
        this.prop("maxid", 0);

        /**
         * Nodes storage (we have plain structure in NestedSets)
         * @property {Array} nodes 
         */
        this.bprop("nodes", []);

        /**
         * Prevents tree to be sorted too early. Tree has to be sorted after
         * all 'left' and 'right' parameters changed of all nodes.
         * @property {bool} cansort Indicates wheter tree can be sorted
         */
        this.bprop("cansort", false);

        /**
         * @constant
         */
        this.storageKey = "IterativeTree";

        /**
         * Tree nodes sorted
         * @property {Array} sorted 
         */
        var self = this;
        this.sorted = ko.computed(function () {
            // can only be sorted while allowed
            if (self.cansort()) {
                return self.nodes().sort(function (left, right) {
                    return left.left() == right.left() ? 0 : left.left() < right.left() ? -1 : 1;
                });
            } else {
                return self.nodes();
            }
        });

        /** init and load tree */
        this.init();
        this.load();
    }

    /**
     * Tree initialization
     * @returns {undefined}
     */

    _createClass(IterativeTree, [{
        key: "init",
        value: function init(name) {
            this.nodes([]);
            this.maxid = 0;
            var node = new IterativeTreeNode();
            node.fromModel({ id: 0, name: name || "Root" });
            this._addNode(node);
        }

        /**
         * Simple unique id generation, used as tree nodes identifier
         * @returns {Number}
         */
    }, {
        key: "_uniqId",
        value: function _uniqId() {
            return ++this.maxid;
        }

        /**
         * Indicates whether left or right parameters is out of bounds of the tree.
         * @param {int} left
         * @param {int} right
         * @returns {Boolean}
         */
    }, {
        key: "_outOfBounds",
        value: function _outOfBounds(left, right) {
            return left < 1 || right > this.nodes().length * 2;
        }

        /**
         * Adds node to a nested set tree, saves the tree state.
         * @param {IterativeTreeNode} node - Node to add
         * @param {int} left - insertion point 'left' indicator
         * @param {int} right - insertion point 'right' indicator
         * @returns {undefined}
         */
    }, {
        key: "_addNode",
        value: function _addNode(node, left, right) {

            // In case we have no nodes, just add a singne node with
            // indicators 1 and 2
            if (this.nodes().length == 0) {
                node.left(1);
                node.right(2);
                this.nodes.push(node);
                return;
            }

            this.cansort(false);

            // According to nested sets method, 'left' cannot be less than 1
            if (left < 1) {
                left = 1;
            }

            // Same to the 'right', it cannot be greater than double number of nodes
            if (right > this.nodes().length * 2) {
                right = this.nodes().length * 2;
            }

            // Find the connecting node
            var connectingNode = this.getNode(left, right);
            if (connectingNode === undefined) {
                // console.warn("Invalid connection point: ", left, right);
                return;
            }

            // Update other nodes parameters
            this.nodes().forEach(function (n) {
                if (n.right() > left) {
                    n.right(n.right() + 2);
                }
                if (n.left() > left) {
                    n.left(n.left() + 2);
                }
            });

            // Update current node parameters also
            node.left(left + 1);
            node.right(left + 2);

            this.cansort(true);

            // Add new node record into our tree nodes array
            this.nodes.push(node);

            // Save tree
            this.save();
        }

        /**
         * Finds the node with 'left' and 'right' parameters.
         * @param {int} left
         * @param {int} right
         * @returns {IterativeTreeNode|undefined}
         */
    }, {
        key: "getNode",
        value: function getNode(left, right) {
            var result = null;

            // Check whether we are out of bounds of a tree
            if (this._outOfBounds(left, right)) {
                return;
            }

            // Search trough stored nodes
            this.nodes().forEach(function (n) {
                if (n.left() === left && n.right() === right) {
                    result = n;return;
                }
            });

            // Found the node
            if (result) {
                return result;
            }

            // connectors can only diff by 1
            if (right - left === 1) {
                return null;
            } // node connector (not supported)

            // or nothing
        }

        /**
         * Adds a new node to provided 'data' node
         * @param {IterativeTreeNode} data
         * @returns {undefined}
         */
    }, {
        key: "add",
        value: function add(data) {
            var id = this._uniqId();
            var node = new IterativeTreeNode();
            node.fromModel({ id: id, name: data.id + '-' + id, parentId: data.id, collapsed: data.collapseChildren() });
            this._addNode(node, data.left(), data.right());
        }

        /**
         * Removes a node from the tree, saves the tree state.
         * @param {IterativeTreeNode} node
         * @returns {undefined}
         */
    }, {
        key: "remove",
        value: function remove(node) {
            var _this = this;

            var left = node.left(),
                right = node.right(),
                width = node.right() - node.left() + 1,
                rname = null;

            this.cansort(false);

            // Remove node and all it's subnodes
            this.nodes().forEach(function (n) {
                if (n.left() >= left && n.right() <= right) {
                    // Keep root node name, as we always should have one
                    if (_this.isRoot(n)) {
                        rname = n.name();
                    }
                    _this.nodes(_.without(_this.nodes(), n));
                }
            });

            // Update nodes parameters
            this.nodes().forEach(function (n) {
                if (n.right() > right) {
                    n.right(n.right() - width);
                }
                if (n.left() > right) {
                    n.left(n.left() - width);
                }
            });

            // If all nodes removed, create new root node
            if (this.nodes().length < 1) {
                this.init(rname);
            }

            this.cansort(true);

            // Save tree
            this.save();
        }

        /**
         * Calculates provided node depth within a tree
         * @param {IterativeTreeNode} node
         * @returns {Number} - depth
         */
    }, {
        key: "depth",
        value: function depth(node) {
            var depth = -1;

            this.nodes().forEach(function (p) {
                if (node.left() >= p.left() && node.left() <= p.right()) {
                    depth++;
                }
            });

            return depth < 0 ? 0 : depth;
        }

        /**
         * Loads tree state from localStorage
         * @returns {undefined}
         */
    }, {
        key: "load",
        value: function load() {
            var data = JSON.parse(localStorage.getItem(this.storageKey));
            if (data) {
                this.maxid = data.maxid || 0;
                if (data.data) {
                    this.fromModel(data.data);
                }
            }
        }

        /**
         * Saves tree state to localStorage
         * @returns {undefined}
         */
    }, {
        key: "save",
        value: function save() {
            localStorage.setItem(this.storageKey, JSON.stringify({ maxid: this.maxid, data: this.toModel() }));
        }

        /**
         * Indicates wheher node is a tree root
         * @param {IterativeTreeNode} data
         * @returns {Boolean}
         */
    }, {
        key: "isRoot",
        value: function isRoot(data) {
            return data.id !== undefined && data.id === 0;
        }

        /**
         * Resets the tree and saves tree state.
         * @returns {undefined}
         */
    }, {
        key: "reset",
        value: function reset() {
            this.init();
            this.save();
        }

        /**
         * Switches tree node into an edit-name mode
         * @param {IterativeTreeNode} data
         * @returns {undefined}
         */
    }, {
        key: "editName",
        value: function editName(data) {
            data.editname(data.name());
            data.edit(true);
        }

        /**
         * Saves name for a given node, switches edit mode for a node off 
         * @param {IterativeTreeNode} data
         * @returns {undefined}
         */
    }, {
        key: "saveName",
        value: function saveName(data) {
            if (data.name() != data.editname()) {
                data.name(data.editname());
                this.save();
            }
            data.edit(false);
        }

        /**
         * Toggles collapse/expand state of the node (subnodes left untouched),
         * saves tree state.
         * @param {IterativeTreeNode} data
         * @returns {undefined}
         */
    }, {
        key: "toggleCollapse",
        value: function toggleCollapse(data) {
            data.collapseChildren(!data.collapseChildren());
            this._collateChildren(data);
            this.save();
        }

        /**
         * Finds node parent
         * @param {IterativeTreeNode} node
         * @returns {IterativeTreeNode}
         */
    }, {
        key: "getParent",
        value: function getParent(node) {
            // never used so far
            return _.find(this.nodes(), function (n) {
                return n.id == node.parentId;
            });
        }

        /**
         * Collapse child nodes of a node
         * @param {IterativeTreeNode} node
         * @returns {undefined}
         */
    }, {
        key: "_collateChildren",
        value: function _collateChildren(node) {
            this.nodes().forEach(function (n) {
                if (n.left() > node.left() && n.right() < node.right()) {
                    n.collapsed(node.collapseChildren());
                    if (!node.collapseChildren()) {
                        n.collapseChildren(false);
                    }
                }
            });
        }

        /**
         * Disposal (if required)
         */
    }, {
        key: "dispose",
        value: function dispose() {
            _get(Object.getPrototypeOf(IterativeTree.prototype), "dispose", this).call(this);
        }
    }]);

    return IterativeTree;
})(CoreModel);
