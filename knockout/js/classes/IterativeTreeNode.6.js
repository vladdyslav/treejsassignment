/*!
 * Iterative Tree node class
 * Describes a tree node object for iterative tree.
 */

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IterativeTreeNode = (function (_TreeNode) {
  _inherits(IterativeTreeNode, _TreeNode);

  /**
   * @constructor
   */

  function IterativeTreeNode() {
    _classCallCheck(this, IterativeTreeNode);

    _get(Object.getPrototypeOf(IterativeTreeNode.prototype), "constructor", this).call(this);

    /**
     * Indicates class type to be instantiated while object is loaded from 
     * model object
     * @property {string} _class
     */
    this.prop("_class", "IterativeTreeNode");

    /**
     * Nested sets node 'left' parameter
     * @property {int} left 'left' parameter
     */
    this.bprop("left", 0);

    /**
     * Nested sets node 'right' parameter
     * @property {int} left 'right' parameter
     */
    this.bprop("right", 0);

    /**
     * Indicates if node children are collapsed
     * @property {bool} collapseChildren
     */
    this.bprop("collapseChildren", false);
  }

  /**
   * Indicates if node has child nodes
   */

  _createClass(IterativeTreeNode, [{
    key: "hasChildren",
    value: function hasChildren() {
      return this.right() - this.left() > 1;
    }

    /**
     * Disposal (if required)
     */
  }, {
    key: "dispose",
    value: function dispose() {
      _get(Object.getPrototypeOf(IterativeTreeNode.prototype), "dispose", this).call(this);
    }
  }]);

  return IterativeTreeNode;
})(TreeNode);
