/*!
 * Recursive Tree class
 * Class to handle and manage tree in a recursive manner.
 * Recursive tree nodes are instances of RecursiveTreeNode type.
 * Class handles saving and loading data to localStorage under 'RecursiveTree' key
 */

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RecursiveTree = (function (_CoreModel) {
  _inherits(RecursiveTree, _CoreModel);

  function RecursiveTree() {
    _classCallCheck(this, RecursiveTree);

    _get(Object.getPrototypeOf(RecursiveTree.prototype), "constructor", this).call(this);

    /**
     * Indicates class type to be instantiated while object is loaded from 
     * model object
     * @property {string} _class
     */
    this.prop("_class", "RecursiveTree");

    /**
     * Counter, used for basic unique id generation for nodes
     * @property {int} maxid
     * @default
     */
    this.prop("maxid", 0);

    /**
     * Root recursive tree node
     * @property {RecursiveTreeNode} data Holds root of the tree
     */
    this.bprop("data");

    /**
     * Indicates key for localStorage, to store data of recursive tree
     * @constant
     * @type {string}
     * @default
     */
    this.storageKey = "RecursiveTree";

    /** init and load tree */
    this.init();
    this.load();
  }

  /**
   * Tree initialization
   * @returns {undefined}
   */

  _createClass(RecursiveTree, [{
    key: "init",
    value: function init() {
      this.maxid = 0;
      var node = new RecursiveTreeNode();

      // build root node from empty model
      node.fromModel({ id: 0, name: "Root", nodes: [] });

      this.data(node);
    }

    /**
     * Simple unique id generation, used as tree nodes identifier
     * @returns {Number}
     */
  }, {
    key: "_uniqId",
    value: function _uniqId() {
      return ++this.maxid;
    }

    /**
     * Switches tree node into an edit-name mode
     * @param {RecursiveTreeNode} data
     * @returns {undefined}
     */
  }, {
    key: "editName",
    value: function editName(data) {
      data.editname(data.name());
      data.edit(true);
    }

    /**
     * Saves name for a given node, switches edit mode for a node off
     * @param {RecursiveTreeNode} data
     * @returns {undefined}
     */
  }, {
    key: "saveName",
    value: function saveName(data) {
      // no need to save if no change
      if (data.name() != data.editname()) {
        data.name(data.editname());
        this.save();
      }
      data.edit(false);
    }

    /**
     * Toggles collapse/expand state of the node (subnodes left untouched),
     * saves tree state.
     * @param {type} data
     * @returns {undefined}
     */
  }, {
    key: "toggleCollapse",
    value: function toggleCollapse(data) {
      data.collapsed(!data.collapsed());
      this.save();
    }

    /**
     * Removes current node from the parent node, saves tree state.
     * @param {RecursiveTreeNode} data - currentnode
     * @param {RecursiveTreeNode} parent - parent node
     * @returns {undefined}
     */
  }, {
    key: "remove",
    value: function remove(data, parent) {
      // remove nodes
      data.nodes([]);

      if (parent && parent.nodes && parent.nodes().length > 0) {
        parent.nodes(_.without(parent.nodes(), data));
      }
      if (this.data().nodes().length === 0) {
        this.maxid = 0;
      }
      this.save();
    }

    /**
     * Adds new node to a tree, assuming current given node as its parent,
     * saves tree state.
     * @param {RecursiveTreeNode} data
     * @returns {undefined}
     */
  }, {
    key: "add",
    value: function add(data) {
      var id = this._uniqId();
      var node = new RecursiveTreeNode();
      node.fromModel({ id: id, name: data.id + '-' + id, nodes: [] });
      data.nodes.push(node);
      this.save();
    }

    /**
     * Loads tree state from localStorage
     * @returns {undefined}
     */
  }, {
    key: "load",
    value: function load() {
      var data = JSON.parse(localStorage.getItem(this.storageKey));
      if (data) {
        this.maxid = data.maxid || 0;
        if (data.data) {
          this.data().fromModel(data.data);
        }
      }
    }

    /**
     * Saves tree state to localStorage
     * @returns {undefined}
     */
  }, {
    key: "save",
    value: function save() {
      localStorage.setItem(this.storageKey, JSON.stringify({ maxid: this.maxid, data: this.data().toModel() }));
    }

    /**
     * Indicates whether provided node is a tree Root.
     * @param {RecursiveTreeNode} data
     * @returns {Boolean}
     */
  }, {
    key: "isRoot",
    value: function isRoot(data) {
      return data.id !== undefined && data.id === 0;
    }

    /**
     * Resets the tree and saves tree state.
     * @returns {undefined}
     */
  }, {
    key: "reset",
    value: function reset() {
      this.init();
      this.save();
    }

    /**
     * Disposal (if required)
     */
  }, {
    key: "dispose",
    value: function dispose() {
      _get(Object.getPrototypeOf(RecursiveTree.prototype), "dispose", this).call(this);
    }
  }]);

  return RecursiveTree;
})(CoreModel);
