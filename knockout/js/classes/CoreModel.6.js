/*!
 * Core data model, used to define properties and bindable properties
 * help convert objects to models and instantiate objects from models, this
 * will be used to store/load data of the objects.
 */

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x3, _x4, _x5) { var _again = true; _function: while (_again) { var object = _x3, property = _x4, receiver = _x5; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x3 = parent; _x4 = property; _x5 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CoreModel = (function (_CoreObject) {
    _inherits(CoreModel, _CoreObject);

    /**
     * @constructor
     */

    function CoreModel() {
        _classCallCheck(this, CoreModel);

        _get(Object.getPrototypeOf(CoreModel.prototype), "constructor", this).call(this);

        /**
         * Storing list of properties which shall be saved/loaded
         * @property {Array} _props List of properties
         */
        this._props = [];

        /**
         * Storing list of bindable properties which shall be saved/loaded
         * @property {Array} _bprops List of bindable properties
         */
        this._bprops = [];

        /**
         * Indicates class type to be instantiated while object is loaded from 
         * model object
         * @property {string} _class
         */
        this.prop("_class", "CoreModel");
    }

    /**
     * Indicates whether object is an observable array
     * @param {object} obj - object to check
     */

    _createClass(CoreModel, [{
        key: "isObservableArray",
        value: function isObservableArray(obj) {
            return ko.isObservable(obj) && obj.remove !== undefined;
        }

        /**
         * Defines a property
         * @param {string} name - name of the property to define
         * @param {object} def - object to be set for this property as a value
         */
    }, {
        key: "prop",
        value: function prop(name) {
            var def = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];

            if (!this.hasOwnProperty(name)) {
                this._props.push(name);
            }
            this[name] = def;
        }

        /**
         * Defines a bindable property
         * @param {string} name - name of the property to define
         * @param {object} def - object to be set for this property as a value
         */
    }, {
        key: "bprop",
        value: function bprop(name) {
            var def = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];

            if (!this.hasOwnProperty(name)) {
                this._bprops.push(name);
            }
            if (_.isArray(def)) {
                this[name] = ko.observableArray(def);
            } else {
                this[name] = ko.observable(def);
            }
        }

        /**
         * Serializes object into a model object
         * @param {object} obj - Object to serialize
         */
    }, {
        key: "_toObject",
        value: function _toObject(obj) {
            if (obj && obj['_class'] && _.isFunction(obj.toModel)) {
                return obj.toModel();
            }
            return obj;
        }

        /**
         * Deserializes object from model object
         * @param {object} obj - Object to deserialize
         */
    }, {
        key: "_fromObject",
        value: function _fromObject(obj) {
            var result;
            if (obj && obj['_class']) {
                result = eval("new " + obj['_class'] + "();");
                result.fromModel(_.omit(obj, ['_class']));
            } else {
                result = obj;
            }
            return result;
        }

        /**
         * Serializes array into a model object
         * @param {Array} arr - array to serialize
         */
    }, {
        key: "_toArray",
        value: function _toArray(arr) {
            var _this = this;

            var result = [];
            arr.forEach(function (a) {
                if (_.isArray(a)) {
                    result.push(_this._toArray(a));
                } else if (_.isObject(a)) {
                    result.push(_this._toObject(a));
                } else {
                    result.push(a);
                }
            });
            return result;
        }

        /**
         * Deserializes array from model object
         * @param {Array} arr - array to deserialize
         */
    }, {
        key: "_fromArray",
        value: function _fromArray(arr) {
            var _this2 = this;

            var result = [];

            arr.forEach(function (a) {
                if (_.isArray(a)) {
                    result.push(_this2._fromArray(a));
                } else if (_.isObject(a)) {
                    result.push(_this2._fromObject(a));
                } else {
                    result.push(a);
                }
            });

            return result;
        }

        /**
         * Converts object into a serializable model object for further saving
         */
    }, {
        key: "toModel",
        value: function toModel() {
            var _this3 = this;

            var result = {};

            this._props.forEach(function (p) {
                if (_.isArray(_this3[p])) {
                    result[p] = _this3._toArray(_this3[p]);
                } else if (_.isObject(_this3[p])) {
                    result[p] = _this3._toObject(_this3[p]);
                } else {
                    result[p] = _this3[p];
                }
            });

            this._bprops.forEach(function (p) {
                if (_this3.isObservableArray(_this3[p])) {
                    result[p] = _this3._toArray(_this3[p]());
                } else if (_.isObject(_this3[p]())) {
                    result[p] = _this3._toObject(_this3[p]());
                } else {
                    result[p] = _this3[p]();
                }
            });

            return result;
        }

        /**
         * Restores object instance from object model
         * @param {object} model - model object
         */
    }, {
        key: "fromModel",
        value: function fromModel(model) {
            _.each(model, function (mv, mk) {
                if (_.contains(this._props, mk)) {
                    // we have this property, try to set
                    if (_.isArray(mv)) {
                        this[mk] = this._fromArray(mv);
                    } else if (_.isObject(mv)) {
                        this[mk] = this._fromObject(mv);
                    } else {
                        this[mk] = mv;
                    }
                }

                if (_.contains(this._bprops, mk)) {
                    // we have this property, try to set
                    if (_.isArray(mv)) {
                        this[mk](this._fromArray(mv));
                    } else if (_.isObject(mv)) {
                        this[mk](this._fromObject(mv));
                    } else {
                        this[mk](mv);
                    }
                }
            }, this);
        }

        /**
         * Try to dispose any array
         * @param {Array} arr Array to dispose
         */
    }, {
        key: "_disposeArray",
        value: function _disposeArray(arr) {
            var _this4 = this;

            if (arr.length == 0) {
                return;
            }
            arr.forEach(function (a) {
                if (_.isArray(a)) {
                    _this4._disposeArray(a);
                }
                if (_.isObject(a)) {
                    _this4._disposeObject(a);
                }
            });
        }

        /**
         * Try to dispose any object
         * @param {object} obj Object to dispose
         */
    }, {
        key: "_disposeObject",
        value: function _disposeObject(obj) {
            if (obj.hasOwnProperty('dispose') && _.isFunction(obj.dispose)) {
                obj.dispose();
            }
        }

        /**
         * Disposal (if required)
         */
    }, {
        key: "dispose",
        value: function dispose() {
            var _this5 = this;

            _get(Object.getPrototypeOf(CoreModel.prototype), "dispose", this).call(this);
            this._props.forEach(function (p) {
                if (_.isArray(_this5[p])) {
                    _this5._disposeArray(_this5[p]);
                }
                if (_.isObject(_this5[p])) {
                    _this5._disposeObject(_this5[p]);
                }
                delete _this5[p];
            });

            this._bprops.forEach(function (p) {
                var a = _this5[p]();
                if (_.isArray(a)) {
                    _this5._disposeArray(a);
                }
                if (_.isObject(a)) {
                    _this5._disposeObject(a);
                }
                delete _this5[p];
            });
        }
    }]);

    return CoreModel;
})(CoreObject);
