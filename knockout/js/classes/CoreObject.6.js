/*!
 * Core object, all classes extended from this class, for further flexibility
 */

'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var CoreObject = (function () {

  /**
   * @constructor
   */

  function CoreObject() {
    _classCallCheck(this, CoreObject);

    /**
     * Indicates unique id of the object
     * @property {int} cid Unique object id
     */
    this.cid = _.uniqueId('c');
  }

  /**
   * Disposal (if required)
   */

  _createClass(CoreObject, [{
    key: 'dispose',
    value: function dispose() {
      delete this.cid;
    }
  }]);

  return CoreObject;
})();
