module.exports = function(config){
  config.set({

    basePath : './',
    
    files : [
      'js/lib/underscore-min.js',
      'js/lib/jquery.min.js',
      'js/lib/bootstrap.min.js',
      'js/lib/knockout-3.2.0.js',
      'node_modules/traceur/bin/traceur-runtime.js',
      'js/classes/CoreObject.6.js',
      'js/classes/CoreModel.6.js',
      'js/classes/TreeNode.6.js',
      'js/classes/IterativeTreeNode.6.js',
      'js/classes/RecursiveTreeNode.6.js',
      'js/classes/IterativeTree.6.js',
      'js/classes/RecursiveTree.6.js',
      'js/classes/App.6.js',
      'src/js/tests/**/*_test.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
