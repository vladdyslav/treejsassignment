#!/bin/sh

ES6PATH="./src/js/classes/"
BUILDPATH="./js/classes/"
COMPILER=`which babel`
NOTFOUND=$?

if [ ${NOTFOUND} == "1" ]; then
    echo "Install babel first: 'npm install babel -g'"
    exit 1
else
    $COMPILER --out-file ${BUILDPATH}CoreObject.6.js ${ES6PATH}CoreObject.6.js
    $COMPILER --out-file ${BUILDPATH}App.6.js ${ES6PATH}App.6.js
    $COMPILER --out-file ${BUILDPATH}CoreModel.6.js ${ES6PATH}CoreModel.6.js
    $COMPILER --out-file ${BUILDPATH}TreeNode.6.js ${ES6PATH}TreeNode.6.js
    $COMPILER --out-file ${BUILDPATH}IterativeTreeNode.6.js ${ES6PATH}IterativeTreeNode.6.js
    $COMPILER --out-file ${BUILDPATH}RecursiveTreeNode.6.js ${ES6PATH}RecursiveTreeNode.6.js
    $COMPILER --out-file ${BUILDPATH}IterativeTree.6.js ${ES6PATH}IterativeTree.6.js
    $COMPILER --out-file ${BUILDPATH}RecursiveTree.6.js ${ES6PATH}RecursiveTree.6.js
fi