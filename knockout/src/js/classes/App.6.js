/*!
 * Main application class.
 * Initializes trees to be demonstrated
 */

class App extends CoreModel {
    /**
     * @constructor
     */
    constructor() {
        super();
        
        this.prop("IterativeTree", new IterativeTree());
        this.prop("RecursiveTree", new RecursiveTree());
    }
    
    /**
     * Disposal (if required)
     */
    dispose() {
       super.dispose();
    }
}