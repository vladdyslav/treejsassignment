/*!
 * Iterative Tree node class
 * Describes a tree node object for iterative tree.
 */

class IterativeTreeNode extends TreeNode {
    
    /**
     * @constructor
     */
    constructor(){
        super();
        
        /**
         * Indicates class type to be instantiated while object is loaded from 
         * model object
         * @property {string} _class
         */
        this.prop("_class", "IterativeTreeNode");
        
        /**
         * Nested sets node 'left' parameter
         * @property {int} left 'left' parameter
         */
        this.bprop("left", 0);
        
        /**
         * Nested sets node 'right' parameter
         * @property {int} left 'right' parameter
         */
        this.bprop("right", 0);
        
        /**
         * Indicates if node children are collapsed
         * @property {bool} collapseChildren
         */
        this.bprop("collapseChildren", false);
    }
    
    /**
     * Indicates if node has child nodes
     */
    hasChildren() {
        return this.right() - this.left() > 1;
    }
    
    /**
     * Disposal (if required)
     */
    dispose(){
        super.dispose();
    }
}
