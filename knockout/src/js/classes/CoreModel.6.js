/*!
 * Core data model, used to define properties and bindable properties
 * help convert objects to models and instantiate objects from models, this
 * will be used to store/load data of the objects.
 */

class CoreModel extends CoreObject {
    
    /**
     * @constructor
     */
    constructor(){
        super();
        
        /**
         * Storing list of properties which shall be saved/loaded
         * @property {Array} _props List of properties
         */
        this._props = [];
        
        /**
         * Storing list of bindable properties which shall be saved/loaded
         * @property {Array} _bprops List of bindable properties
         */
        this._bprops = [];
        
        /**
         * Indicates class type to be instantiated while object is loaded from 
         * model object
         * @property {string} _class
         */
        this.prop("_class", "CoreModel");
    }
    
    /**
     * Indicates whether object is an observable array
     * @param {object} obj - object to check
     */
    isObservableArray( obj ) {
        return ( ko.isObservable(obj) && obj.remove !== undefined);
    }
    
    /**
     * Defines a property
     * @param {string} name - name of the property to define
     * @param {object} def - object to be set for this property as a value
     */
    prop (name, def = undefined) {
        if (!this.hasOwnProperty(name)) { this._props.push(name); }
        this[name] = def;
    }
    
    /**
     * Defines a bindable property
     * @param {string} name - name of the property to define
     * @param {object} def - object to be set for this property as a value
     */
    bprop (name, def = undefined) {
        if (!this.hasOwnProperty(name)) { this._bprops.push(name); }
        if (_.isArray(def)) {
            this[name] = ko.observableArray(def);
        } else {
            this[name] = ko.observable(def);
        }
    }
    
    /**
     * Serializes object into a model object
     * @param {object} obj - Object to serialize
     */
    _toObject(obj) {
        if (obj && obj['_class'] && _.isFunction(obj.toModel)) { return obj.toModel(); }
        return obj;
    }
    
    /**
     * Deserializes object from model object
     * @param {object} obj - Object to deserialize
     */
    _fromObject(obj) {
        var result;
        if (obj && obj['_class']) { 
            result = eval("new " + obj['_class'] + "();");
            result.fromModel(_.omit(obj, ['_class']));
        } else {
            result = obj;
        }
        return result;
    }
    
    /**
     * Serializes array into a model object
     * @param {Array} arr - array to serialize
     */
    _toArray(arr) {
        var result = [];
        arr.forEach((a) => {
            if (_.isArray(a)) { result.push(this._toArray(a)); }
            else if (_.isObject(a)) { result.push(this._toObject(a)); }
            else { result.push(a); }
        });
        return result;
    }
    
    /**
     * Deserializes array from model object
     * @param {Array} arr - array to deserialize
     */
    _fromArray(arr) {
        var result = [];
        
        arr.forEach((a) => {
            if (_.isArray(a)) {result.push(this._fromArray(a));}
            else if (_.isObject(a)) { result.push(this._fromObject(a)); }
            else { result.push(a); }
        });
        
        return result;
    }
    
    /**
     * Converts object into a serializable model object for further saving
     */
    toModel() {
        var result = {};
        
        this._props.forEach((p) => {
            if (_.isArray(this[p])) { result[p] = this._toArray(this[p]); }
            else if (_.isObject(this[p])) { result[p] = this._toObject(this[p]); }
            else { result[p] = this[p]; }
        });
                
        this._bprops.forEach((p) => {
            if (this.isObservableArray(this[p])) {result[p] = this._toArray(this[p]());}
            else if (_.isObject(this[p]())) { result[p] = this._toObject(this[p]()); }
            else { result[p] = this[p](); }
        });
        
        return result;
    }
    
    /**
     * Restores object instance from object model
     * @param {object} model - model object
     */
    fromModel(model) {
        _.each(model, function(mv, mk){
            if (_.contains(this._props, mk)) {
                // we have this property, try to set
                if (_.isArray(mv)) { this[mk] = this._fromArray(mv); }
                else if (_.isObject(mv)) {this[mk] = this._fromObject(mv); }
                else { this[mk] = mv; }
            }
            
            if (_.contains(this._bprops, mk)) {
                // we have this property, try to set
                if (_.isArray(mv)) { this[mk](this._fromArray(mv)); }
                else if (_.isObject(mv)) {this[mk](this._fromObject(mv)); }
                else { this[mk](mv); }
            }
            
        }, this);
    }
    
    /**
     * Try to dispose any array
     * @param {Array} arr Array to dispose
     */
    _disposeArray(arr) {
        if (arr.length == 0) {return;}
        arr.forEach((a) => {
            if (_.isArray(a)) { this._disposeArray(a); }
            if (_.isObject(a)) { this._disposeObject(a); }
        });
    }
    
    /**
     * Try to dispose any object
     * @param {object} obj Object to dispose
     */
    _disposeObject(obj) {
        if (obj.hasOwnProperty('dispose') && _.isFunction(obj.dispose)) { obj.dispose(); }
    }
    
    /**
     * Disposal (if required)
     */
    dispose(){
        super.dispose();
        this._props.forEach((p) => {
            if (_.isArray(this[p])) { this._disposeArray(this[p]); }
            if (_.isObject(this[p])) { this._disposeObject(this[p]); }
            delete this[p]
        });
        
        this._bprops.forEach((p) => {
            let a = this[p]();
            if (_.isArray(a)) { this._disposeArray(a); }
            if (_.isObject(a)) { this._disposeObject(a); }
            delete this[p]
        });
    }
}