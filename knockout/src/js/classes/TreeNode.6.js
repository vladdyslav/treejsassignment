/*!
 * Tree node class
 * Describes base tree node.
 */

class TreeNode extends CoreModel {
    constructor(){
        super();
        
        /**
         * Indicates class type to be instantiated while object is loaded from 
         * model object
         * @property {string} _class
         */
        this.prop("_class", "TreeNode");
        
        /**
         * Holds id of the node
         * @property {int} id 
         */
        this.prop("id", 0);
        
        /**
         * Holds parent id of the node
         * Note! Not used for IterativeTreeNodes
         * @property {int} parentId 
         */
        this.prop("parentId", null);
        
        /**
         * Name of the node displayed to the user
         * @property {string} name Node name
         */
        this.bprop("name", "");
        
        /**
         * Node collapsed state
         * @property {bool} collapsed
         */
        this.bprop("collapsed", false);
        
        /**
         * Node editing state
         * @property {bool} edit
         */
        this.edit = ko.observable(false);
        
        /**
         * Storage for user temporary entered node name
         * @property {string} editname Holds temporary user-entered node name (has to be saved)
         */
        this.editname = ko.observable("");
    }
    
    /**
     * Disposal (if required)
     */
    dispose(){
        super.dispose();
    }
}