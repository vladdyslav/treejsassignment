/*!
 * Recursive Tree node class
 * Describes a tree node object for recursive tree.
 */

class RecursiveTreeNode extends TreeNode {
    
    constructor(){
        super();
        
        /**
         * Indicates class type to be instantiated while object is loaded from 
         * model object
         * @property {string} _class
         */
        this.prop("_class", "RecursiveTreeNode");
        
        /**
         * Child nodes storage
         * @property {Array} nodes Array of RecursiveTreeNode
         */
        this.bprop("nodes", []);
    }
    
    /**
     * Indicates whether node has child nodes
     * @returns {bool}
     */
    hasChildren() {
        return this.nodes().length > 0;
    }
    
    /**
     * Indicates whether node has parent node
     * @returns {bool}
     */
    hasParent() {
        return this.parentId !== null;
    }
    
    /**
     * Disposal (if required)
     */
    dispose(){
        super.dispose();
    }
    
}