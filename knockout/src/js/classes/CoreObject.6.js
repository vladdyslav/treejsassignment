/*!
 * Core object, all classes extended from this class, for further flexibility
 */

class CoreObject {
    
    /**
     * @constructor
     */
    constructor() {
        
        /**
         * Indicates unique id of the object
         * @property {int} cid Unique object id
         */
        this.cid = _.uniqueId('c');
    }
    
    /**
     * Disposal (if required)
     */
    dispose() {
       delete this.cid; 
    }
}