/*!
 * Recursive Tree class
 * Class to handle and manage tree in a recursive manner.
 * Recursive tree nodes are instances of RecursiveTreeNode type.
 * Class handles saving and loading data to localStorage under 'RecursiveTree' key
 */

class RecursiveTree extends CoreModel {
    
    constructor(){
        super();
        
        /**
         * Indicates class type to be instantiated while object is loaded from 
         * model object
         * @property {string} _class
         */
        this.prop("_class", "RecursiveTree");
        
        /**
         * Counter, used for basic unique id generation for nodes
         * @property {int} maxid
         * @default
         */
        this.prop("maxid", 0);
        
        /**
         * Root recursive tree node
         * @property {RecursiveTreeNode} data Holds root of the tree
         */
        this.bprop("data");
        
        /**
         * Indicates key for localStorage, to store data of recursive tree
         * @constant
         * @type {string}
         * @default
         */
        this.storageKey = "RecursiveTree";
        
        /** init and load tree */
        this.init();
        this.load();
    }
    
    /**
     * Tree initialization
     * @returns {undefined}
     */
    init () {
        this.maxid = 0;
        var node = new RecursiveTreeNode();
        
        // build root node from empty model
        node.fromModel({id: 0, name: "Root", nodes: []});
        
        this.data(node);
    }
    
    /**
     * Simple unique id generation, used as tree nodes identifier
     * @returns {Number}
     */
    _uniqId() {
        return ++(this.maxid);
    }
    
    /**
     * Switches tree node into an edit-name mode
     * @param {RecursiveTreeNode} data
     * @returns {undefined}
     */
    editName (data) {
        data.editname(data.name());
        data.edit(true);
    }
    
    /**
     * Saves name for a given node, switches edit mode for a node off
     * @param {RecursiveTreeNode} data
     * @returns {undefined}
     */
    saveName (data) {
        // no need to save if no change
        if (data.name() != data.editname()) {
            data.name(data.editname());
            this.save();
        }
        data.edit(false);
    }
    
    /**
     * Toggles collapse/expand state of the node (subnodes left untouched),
     * saves tree state.
     * @param {type} data
     * @returns {undefined}
     */
    toggleCollapse (data) {
        data.collapsed(!data.collapsed());
        this.save();
    }
    
    /**
     * Removes current node from the parent node, saves tree state.
     * @param {RecursiveTreeNode} data - currentnode
     * @param {RecursiveTreeNode} parent - parent node
     * @returns {undefined}
     */
    remove (data, parent) {
        // remove nodes
        data.nodes([]);

        if (parent && parent.nodes && parent.nodes().length > 0) { parent.nodes(_.without(parent.nodes(), data)); }
        if (this.data().nodes().length === 0) { this.maxid = 0; }
        this.save();
    }
    
    /**
     * Adds new node to a tree, assuming current given node as its parent,
     * saves tree state.
     * @param {RecursiveTreeNode} data
     * @returns {undefined}
     */
    add (data) {
        var id = this._uniqId();
        var node = new RecursiveTreeNode();
        node.fromModel({id: id, name: data.id + '-' + id, nodes: []});
        data.nodes.push(node);
        this.save();
    }
    
    /**
     * Loads tree state from localStorage
     * @returns {undefined}
     */
    load () {
        var data = JSON.parse(localStorage.getItem(this.storageKey));
        if (data) {
            this.maxid = data.maxid || 0;
            if (data.data) { this.data().fromModel(data.data); }
        }
    }
    
    /**
     * Saves tree state to localStorage
     * @returns {undefined}
     */
    save () {
        localStorage.setItem(this.storageKey, JSON.stringify({maxid: this.maxid, data: this.data().toModel()}));
    }
    
    /**
     * Indicates whether provided node is a tree Root.
     * @param {RecursiveTreeNode} data
     * @returns {Boolean}
     */
    isRoot(data) {
        return data.id !== undefined && data.id === 0;
    }
    
    /**
     * Resets the tree and saves tree state.
     * @returns {undefined}
     */
    reset() {
        this.init();
        this.save();
    }
    
    /**
     * Disposal (if required)
     */
    dispose(){
        super.dispose();
    }
}

        