'use strict';

describe('CoreModel Tests', function() {

  var app;
  
  /**
   * Init every test in the scope with following
   */
  beforeEach(function(){
      app = new App();
  });
  
  /**
   * CleanUp
   */
  afterEach(function(){
      app.dispose();
  });
  
  // Tests of a core model
  it("CoreModel Tests", function(){
      
      // Create new CoreModel for tests
      var m = new CoreModel();
      
      // Test property creation
      m.prop("name", "CoreName");
      expect(m.name).toBe("CoreName");
      
      // Test observable property creation
      m.bprop("oname", "CoreName");
      expect(ko.isObservable(m.oname)).toBe(true);
      expect(m.oname()).toBe("CoreName");
      
      // Test observable array property creation
      m.bprop("oarr", []);
      expect(m.isObservableArray(m.oarr)).toBe(true);
      
      // Convert CoreModel to model object (for further serializing)
      var model = m.toModel();
      
      // Model tests
      expect(model._class).toBe('CoreModel');
      expect(model.name).toBe('CoreName');
      expect(model.oname).toBe('CoreName');
      expect(_.isArray(model.oarr)).toBe(true);
      
      // Create another core model for tests
      var nm = new CoreModel();
      
      // Define some properties for test
      nm.prop("name", "");
      nm.bprop("oname", "");
      nm.bprop("oarr", []);
      
      // Load data from model Object
      var res = {_class: 'CoreModel', name: 'CoreName', oname: 'CoreName', oarr: []};
      nm.fromModel(res);
      
      // Check what has been loaded
      expect(ko.isObservable(nm.oname)).toBe(true);
      expect(nm.oname()).toBe("CoreName");
      expect(nm.isObservableArray(m.oarr)).toBe(true);
      
      // Check that we have property
      expect(nm.hasOwnProperty('name')).toBe(true);
      
      // dispose models
      nm.dispose();
      m.dispose();
      
      // Check if known properties are disposed
      expect(nm.hasOwnProperty('name')).toBe(false);
      expect(nm.hasOwnProperty('oname')).toBe(false);
      expect(nm.hasOwnProperty('oarr')).toBe(false);
      
  });
  
});