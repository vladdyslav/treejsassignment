'use strict';

var testName = "RecursiveTree";

describe(testName + ' Tests', function() {

    var app, scope;
    
    /**
     * Init every test in the scope with following
     */
    beforeEach(function() {
        app = new App();

    });
    
    /**
     * CleanUp
     */
    afterEach(function(){
        app.dispose();
    });

    describe(testName + " :: Tests", function() {

        /**
         * Init every test in the scope with following
         */
        beforeEach(function() {
            scope = app.RecursiveTree;
            scope.reset();
        });
        
        /**
         * CleanUp
         */
        afterEach(function(){
            scope.dispose();
        });

        it(testName + ' :: isArray', function() {
            expect(_.isArray(scope.data().nodes())).toBe(true);
        });
        
        it(testName + ' :: Must have a node on init', function(){
            expect(scope.data()).not.toBe(undefined);
            expect(scope.data().nodes().length).toBe(0);
        });

    });
    
    
    describe(testName + ' :: Node manipulations', function() {
        
        /**
         * Init every test in the scope with following
         */
        beforeEach(function() {
            scope = app.RecursiveTree;
            scope.reset();
        });
        
        /**
         * CleanUp
         */
        afterEach(function(){
            scope.dispose();
        });
        
        // Simple node manipulation tests
        it(testName + ' :: Simple node manipulations', function(){
            
            // Adding node to root
            scope.add(scope.data());
            
            expect(scope.data().nodes().length).toBe(1);
            
            // Adding 2 more
            scope.add(scope.data());
            scope.add(scope.data());
            
            expect(scope.data().nodes().length).toBe(3);
            
            // Removing node
            scope.remove(scope.data().nodes()[1], scope.data());
            
            expect(scope.data().nodes().length).toBe(2);
        });
        
        // Advanced node manipulation tests
        it(testName + ' :: Advanced node manipulations', function(){
            
            // Adding node to root
            scope.add(scope.data());
            
            // Adding few more
            for(var i = 0; i < 10; i++) {
                scope.add(scope.data().nodes()[0]);
            };
            
            // checking length, must be 10
            expect(scope.data().nodes()[0].nodes().length).toBe(10);
            
            // removing one (has to be removed with all children)
            scope.remove(scope.data().nodes()[0], scope.data());
            
            // checking length, must be 0
            expect(scope.data().nodes().length).toBe(0);
        });
        
    });

});