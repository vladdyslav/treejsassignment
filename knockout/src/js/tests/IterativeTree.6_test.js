/*!
 * Iterative tree tests
 */

'use strict';

var testName = "IterativeTree";

describe(testName + ' tests', function() {

   var app, scope;
   
   /**
   * Init every test in the scope with following
   */
   beforeEach(function() {
        app = new App();
   });
   
   /**
   * CleanUp
   */
   afterEach(function(){
       app.dispose();
   });

  describe(testName + ' controller', function(){
    
    /**
     * Init every test in the scope with following
     */
    beforeEach(function() {
        scope = app.IterativeTree;
        
        // reset the three before each test
        scope.reset();
    });
    
    /**
     * CleanUp
     */
    afterEach(function(){
        scope.dispose();
    });
    
    // Check tree nodes
    describe(testName + ' :: Nodes checks', function(){
        
        // Check if node storage property is array
        it(testName + ' :: isArray', function(){
            expect(_.isArray(scope.nodes())).toBe(true);
        });
    
        // Initialized tree must have a root initialized tree node
        it(testName + ' :: Must have at least one node on init', function(){
            expect(scope.nodes().length).toBeGreaterThan(0);
        });
        
    });
    
    // Node manipulation tests
    describe(testName + ' :: Node manipulations', function() {
        
        // Simple node manipulation tests
        it(testName + ' :: Simple node manipulations', function(){
            
            // Adding first node to root
            scope.add(scope.nodes()[0]);
            
            // Check root node parameters
            expect(scope.nodes()[0].left()).toBe(1);
            expect(scope.nodes()[0].right()).toBe(4);
            
            // Adding 2 more
            scope.add(scope.nodes()[0]);
            scope.add(scope.nodes()[0]);
            
            // Check root node parameters
            expect(scope.nodes()[0].left()).toBe(1);
            expect(scope.nodes()[0].right()).toBe(8);
            
             // Check first added node parameters
            expect(scope.nodes()[1].left()).toBe(2);
            expect(scope.nodes()[1].right()).toBe(3);
            
            // Check second added node parameters
            expect(scope.nodes()[2].left()).toBe(4);
            expect(scope.nodes()[2].right()).toBe(5);
            
            // Check third added node parameters
            expect(scope.nodes()[3].left()).toBe(6);
            expect(scope.nodes()[3].right()).toBe(7);
            
            // Check depth
            expect(scope.depth(scope.nodes()[3])).toBe(1);
            
            // Removing one node
            scope.remove(scope.nodes()[1]);
            
            // Check left nodes parameters
            expect(scope.nodes()[0].left()).toBe(1);
            expect(scope.nodes()[0].right()).toBe(6);
            
            expect(scope.nodes()[1].left()).toBe(2);
            expect(scope.nodes()[1].right()).toBe(3);
            
            expect(scope.nodes()[2].left()).toBe(4);
            expect(scope.nodes()[2].right()).toBe(5);
            
        });
        
        // Advanced node manipulations
        it(testName + ' :: Advanced node manipulations', function(){
            
            // Adding node to root
            scope.add(scope.nodes()[0]);
            
            // Adding few more
            for(var i = 0; i < 10; i++) {
                scope.add(scope.nodes()[1]);
            };
            
            // Check added nodes parameters
            expect(scope.nodes()[0].left()).toBe(1);
            expect(scope.nodes()[0].right()).toBe(24);
            
            expect(scope.nodes()[1].left()).toBe(2);
            expect(scope.nodes()[1].right()).toBe(23);
            
            expect(scope.nodes()[2].left()).toBe(3);
            expect(scope.nodes()[2].right()).toBe(4);
            
            expect(scope.nodes()[3].left()).toBe(5);
            expect(scope.nodes()[3].right()).toBe(6);
            
            // checking length, must be 12 noe
            expect(scope.nodes().length).toBe(12);
            
            // checking depth, has to be 2 for 4th item
            expect(scope.depth(scope.nodes()[3])).toBe(2);
            
            // removing one (has to be removed with all children)
            scope.remove(scope.nodes()[1]);
            
            // checking length, must be only a root node
            expect(scope.nodes().length).toBe(1);
            
            expect(scope.nodes()[0].left()).toBe(1);
            expect(scope.nodes()[0].right()).toBe(2);
        });
        
    });
    
    // Other tests
    describe(testName + ' :: Other tests', function(){
        
        // Expect first node to be root
        it(testName + ' :: isRoot', function(){
            expect(scope.isRoot(scope.nodes()[0])).toBe(true);
        });
        
        // Test if data is correctly prepared for storage
        it(testName + ' :: toModel', function(){
            
            // Get model object of a node
            var model = scope.toModel();
            
            // Check for items stored in model, root has to be there already
            expect(model.nodes.length).toBeGreaterThan(0);
            
            var keys = _.keys(model.nodes[0]);
            
            // Check model data
            expect(keys).toContain('id');
            expect(keys).toContain('parentId');
            expect(keys).toContain('name');
            expect(keys).toContain('left');
            expect(keys).toContain('right');
            expect(keys).toContain('collapsed');
            expect(keys).toContain('collapseChildren');
            
            // first node id has to be 0
            expect(model.nodes[0].id).toBe(0);
            
            // Parent id of the root has to be null
            expect(model.nodes[0].parentId).toBe(null);
            
            // Default root node name has to be 'Root'
            expect(model.nodes[0].name).toBe('Root');
            
            // For single node left and right has to be 1 and 2
            expect(model.nodes[0].left).toBe(1);
            expect(model.nodes[0].right).toBe(2);
            
            // Just created node has not to be collapsed
            expect(model.nodes[0].collapsed).toBe(false);
            expect(model.nodes[0].collapseChildren).toBe(false); 
        });
    });
  });
});
