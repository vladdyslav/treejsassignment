# README #

### What is this repository for? ###

* JS Assignment
* Version 1.0

### About ###
Assignment is complete in two approaches, using Angular and using Knockout.
Each project reside in a separate directory **angular/** and **knockout/** accordingly.

Both projects have two trees solutions: Recursive and Iterative. 
Iterative solution specifically completed using **NestedSets** to demonstrate implementation.

More about NestedSets could be read here:

* https://en.wikipedia.org/wiki/Nested_set_model
* http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/

### How do I get set up? ###

* Prerequisites
    * You have 'nodejs' to be installed
    * You have 'babel' npm module to be installed (**'npm install babel -g'**)

* How to run tests
    * You can do tests by running  **'npm run test-single-run'** from angular/ or knockout/ folder.

* Deployment instructions
    * Go to **angular/** or **knockout/** directory depending which project you are checking
    * run **'npm install'**
    * For knockout ES6 is used so first recompile the sources just in case **'npm run build'** (you may have to install babel compiler first by **'npm install babel -g'**)
    * start the application **'npm start'**
    * point your browser to **'localhost:8000'**

### Notes ###
* Project is developed and tested using latest Chrome, had no deep testing on other browsers.
* Output of angular and knockout looks identical, note the title while testing.


### Feedback / Contacts ###
* You can contact me on **vladdyslav-at-gmail-dot-com**