module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'js/lib/underscore-min.js',
      'js/lib/angular.min.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'js/lib/ui-bootstrap.min.js',
      'js/modules/treeModule/tree-module.js',
      'js/modules/treeModule/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
