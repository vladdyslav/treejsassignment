/*!
 * Recursive tree tests
 */

'use strict';

describe('treeModule RecursiveTree tests', function() {

  beforeEach(module('treeModule'));

  // Tests initialization
  var ctrl, scope, controllerName = "RecursiveTree";
  
  /**
   * Init every test in the scope with following
   */
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    ctrl = $controller(controllerName, { $scope: scope });
  }));
  
  /**
   * CleanUp
   */
  afterEach(function(){ scope = null; ctrl = null; });

  describe(controllerName + ' controller', function(){
    
    // reset the three before each test
    beforeEach(function(){
        scope.reset();
    });
    
    // Check tree nodes
    describe(controllerName + ' :: Nodes checks', function(){
        
        // Check if tree node property is array
        it(controllerName + ' :: isArray', function(){
            expect(_.isArray(scope.data.nodes)).toBe(true);
        });
        
        // Initialized tree tests
        it(controllerName + ' :: Must have a node on init', function(){
            // Initialized tree must have a root initialized tree node 
            expect(scope.data).not.toBe(undefined);
            // And no child nodes
            expect(scope.data.nodes.length).toBe(0);
        });
    });
    
    // Node manipulation tests
    describe(controllerName + ' :: Node manipulations', function() {
        
        // Simple manipulations
        it(controllerName + ' :: Simple node manipulations', function(){
            
            // Adding node to root
            scope.add(scope.data);
            
            // Expect one added
            expect(scope.data.nodes.length).toBe(1);
            
            // Adding 2 more
            scope.add(scope.data);
            scope.add(scope.data);
            
            // Expect 2 more added
            expect(scope.data.nodes.length).toBe(3);
            
            // removing one
            scope.remove(scope.data.nodes[1], scope.data);
            
            // Check if removed
            expect(scope.data.nodes.length).toBe(2);
            
        });
        
        // Advanced node manipulations
        it(controllerName + ' :: Advanced node manipulations', function(){
            
            // Adding node to root
            scope.add(scope.data);
            
            // Adding few more
            for(var i = 0; i < 10; i++) {
                scope.add(scope.data.nodes[0]);
            };
            
            // checking length, must be 10
            expect(scope.data.nodes[0].nodes.length).toBe(10);
            
            // removing one (has to be removed with all children)
            scope.remove(scope.data.nodes[0], scope.data);
            
            // checking length, must be 0
            expect(scope.data.nodes.length).toBe(0);
        });
        
    });
    
    // Other recursive tree tests
    describe(controllerName + ' :: Other tests', function(){
        
        // Test if node is root
        it(controllerName + ' :: isRoot', function(){
            expect(scope.isRoot(scope.data)).toBe(true);
        });
        
        // Test if data is correctly prepared for storage
        it(controllerName + ' :: toModel', function(){
            
            // Get model object of a node
            var model = scope.data.toModel();
            
            // Check that has no children (as tree is just created)
            expect(model.nodes.length).toBe(0);
            
            var keys = _.keys(model);
            
            // Check model data
            expect(keys).toContain('id');
            expect(keys).toContain('name');
            expect(keys).toContain('nodes');
            expect(keys).toContain('collapsed');
            
            // first node id has to be 0
            expect(model.id).toBe(0);
            
            // default root node name has to be 'Root'
            expect(model.name).toBe('Root');
            
            // new node should have no children but has to be an array
            expect(_.isArray(model.nodes)).toBe(true);
            
            // new node is not collapsed
            expect(model.collapsed).toBe(false);
            
        });
        
        // Test if data is correctly loaded from object model
        it(controllerName + ' :: fromModel', function(){
            
            // Prepare a model
            var model =  {
                    id: 0,
                    name: "Root",
                    collapsed: false,
                    nodes: [
                        {
                            id: 1, 
                            name: "2-3",
                            collapsed: false,
                            nodes: []
                        }
                    ]
                };
            
            // Load data
            scope.data.fromModel(model);
            
            // Should have one child
            expect(scope.data.nodes.length).toBe(1);
            
            // Get the child
            var node = scope.data.nodes[0];
            
            // Check child properties
            expect(node.id).toBe(1);
            expect(node.name).toBe('2-3');
            expect(node.collapsed).toBe(false);
            expect(_.isArray(node.nodes)).toBe(true);
        });
        
    });

  });
});
