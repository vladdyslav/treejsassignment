/*!
 * Recursive Tree controller
 * Angular controller, to handle and manage tree in a recursive manner.
 * Recursive tree nodes are instances of RecursiveTreeNode type.
 * Controller handles saving and loading data to localStorage under 'recursiveStorageKey' defined in values/tree-values.js
 */

'use strict';

angular.module("treeModule")
.controller("RecursiveTree", ["$scope", "RecursiveTreeNode", "recursiveStorageKey", function($scope, RecursiveTreeNode, recursiveStorageKey) {
        /**
         * Tree initialization
         * @returns {undefined}
         */
        $scope.init = function() {
            /**
             * Counter, used for basic unique id generation for nodes
             * @default
             */
            $scope.maxid = 0;
            
            /**
             * Root recursive tree node
             * @default
             */
            $scope.data = new RecursiveTreeNode({id: 0, name: "Root", nodes: []});
        };
        
        /**
         * Simple unique id generation, used as tree nodes identifier
         * @returns {Number}
         */
        $scope._uniqId = function() {
            return ++($scope.maxid);
        };
        
        /**
         * Switches tree node into an edit-name mode
         * @param {RecursiveTreeNode} data
         * @returns {undefined}
         */
        $scope.editName = function(data) {
            data.editname = data.name;
            data.edit = true;
        };
        
        /**
         * Saves name for a given node, switches edit mode for a node off 
         * @param {RecursiveTreeNode} data
         * @returns {undefined}
         */
        $scope.saveName = function(data) {
            // no need to save if no change
            if (data.name != data.editname) {
                data.name = data.editname;
                $scope.save();
            }
            data.edit = false;
        };
        
        /**
         * Toggles collapse/expand state of the node (subnodes left untouched),
         * saves tree state.
         * @param {type} data
         * @returns {undefined}
         */
        $scope.toggleCollapse = function(data) {
            data.collapsed = !data.collapsed;
            $scope.save();
        };
        
        /**
         * Removes current node from the parent node, saves tree state.
         * @param {RecursiveTreeNode} data - currentnode
         * @param {RecursiveTreeNode} parent - parent node
         * @returns {undefined}
         */
        $scope.remove = function(data, parent) {
            // remove nodes
            data.nodes = [];

            if (parent) { parent.nodes = _.without(parent.nodes, data); }
            if ($scope.data.nodes.length === 0) { $scope.maxid = 0; }
            $scope.save();
        };
        
        /**
         * Adds new node to a tree, assuming current given node as its parent,
         * saves tree state.
         * @param {RecursiveTreeNode} data
         * @returns {undefined}
         */
        $scope.add = function(data) {
            var id = $scope._uniqId();
            data.nodes.push(new RecursiveTreeNode({id: id, name: data.id + '-' + id, nodes: []}));
            $scope.save();
        };
        
        /**
         * Loads tree state from localStorage
         * @returns {undefined}
         */
        $scope.load = function() {
            var data = JSON.parse(localStorage.getItem(recursiveStorageKey));
            if (data) {
                $scope.maxid = data.maxid || 0;
                if (data.data) {
                    $scope.data.fromModel(data.data);
                }
            }
        };
        
        /**
         * Saves tree state to localStorage
         * @returns {undefined}
         */
        $scope.save = function() {
            localStorage.setItem(recursiveStorageKey, JSON.stringify({maxid: $scope.maxid, data: $scope.data.toModel()}));
        };
        
        /**
         * Indicates whether provided node is a tree Root.
         * @param {RecursiveTreeNode} data
         * @returns {Boolean}
         */
        $scope.isRoot = function(data) {
            return data.id !== undefined && data.id === 0;
        };
        
        /**
         * Resets the tree and saves tree state.
         * @returns {undefined}
         */
        $scope.reset = function(){
            $scope.init();
            $scope.save();
        };
        
        /** init and load tree */
        $scope.init();
        $scope.load();
    }]);
