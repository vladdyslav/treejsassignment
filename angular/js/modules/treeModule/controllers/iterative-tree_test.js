/*!
 * Iterative tree tests
 */

'use strict';

describe('treeModule IterativeTree tests', function() {

  beforeEach(module('treeModule'));

  // Tests initialization
  var ctrl, scope, controllerName = "IterativeTree";
  
  /**
   * Init every test in the scope with following
   */
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    ctrl = $controller(controllerName, { $scope: scope });
  }));
  
  /**
   * CleanUp
   */
  afterEach(function(){ scope = null; ctrl = null; });

  describe(controllerName + ' controller', function(){
    
    // reset the three before each test
    beforeEach(function(){
        scope.reset();
    });
    
    // Check tree nodes
    describe(controllerName + ' :: Nodes checks', function(){
        
        // Check if node storage property is array
        it(controllerName + ' :: isArray', function(){
            expect(_.isArray(scope.nodes)).toBe(true);
        });
        
        // Initialized tree must have a root initialized tree node 
        it(controllerName + ' :: Must have at least one node on init', function(){
            expect(scope.nodes.length).toBeGreaterThan(0);
        });
        
    });
    
    // Node manipulation tests
    describe(controllerName + ' :: Node manipulations', function() {
        
        // Simple node manipulation tests
        it(controllerName + ' :: Simple node manipulations', function(){
            
            // Adding first node to root
            scope.add(scope.nodes[0]);
            
            // Check root node parameters
            expect(scope.nodes[0].left).toBe(1);
            expect(scope.nodes[0].right).toBe(4);
            
            // Adding 2 more
            scope.add(scope.nodes[0]);
            scope.add(scope.nodes[0]);
            
            // Check root node parameters
            expect(scope.nodes[0].left).toBe(1);
            expect(scope.nodes[0].right).toBe(8);
            
            // Check first added node parameters
            expect(scope.nodes[1].left).toBe(6);
            expect(scope.nodes[1].right).toBe(7);
            
            // Check second added node parameters
            expect(scope.nodes[2].left).toBe(4);
            expect(scope.nodes[2].right).toBe(5);
            
            // Check third added node parameters
            expect(scope.nodes[3].left).toBe(2);
            expect(scope.nodes[3].right).toBe(3);
            
            // Check depth
            expect(scope.depth(scope.nodes[3])).toBe(1);
            
            // Removing one node
            scope.remove(scope.nodes[1]);
            
            // Check left nodes parameters
            expect(scope.nodes[0].left).toBe(1);
            expect(scope.nodes[0].right).toBe(6);
            expect(scope.nodes[1].left).toBe(4);
            expect(scope.nodes[1].right).toBe(5);
            expect(scope.nodes[2].left).toBe(2);
            expect(scope.nodes[2].right).toBe(3);
            
        });
        
        // Advanced node manipulations
        it(controllerName + ' :: Advanced node manipulations', function(){
            
            // Adding first node to root
            scope.add(scope.nodes[0]);
            
            // Adding few more
            for(var i = 0; i < 10; i++) {
                scope.add(scope.nodes[1]);
            };
            
            // Check root and first three nodes parameters
            expect(scope.nodes[0].left).toBe(1);
            expect(scope.nodes[0].right).toBe(24);
            
            expect(scope.nodes[1].left).toBe(2);
            expect(scope.nodes[1].right).toBe(23);
            
            expect(scope.nodes[2].left).toBe(21);
            expect(scope.nodes[2].right).toBe(22);
            
            expect(scope.nodes[3].left).toBe(19);
            expect(scope.nodes[3].right).toBe(20);
            
            // Checking length, must be 12 now
            expect(scope.nodes.length).toBe(12);
            
            // Checking depth, has to be 2 for 4th item
            expect(scope.depth(scope.nodes[3])).toBe(2);
            
            // Removing one node (has to be removed with all children)
            scope.remove(scope.nodes[1]);
            
            // Checking length, must be only a root node
            expect(scope.nodes.length).toBe(1);
            
            // Checking root node parameters
            expect(scope.nodes[0].left).toBe(1);
            expect(scope.nodes[0].right).toBe(2);
        });
        
    });
    
    // Other iterative tree tests
    describe(controllerName + ' :: Other tests', function(){
        
        // First node has to be root
        it(controllerName + ' :: isRoot', function(){
            expect(scope.isRoot(scope.nodes[0])).toBe(true);
        });
        
        // Test if data is correctly prepared for storage
        it(controllerName + ' :: toModel', function(){
            
            // Get model object of a node
            var model = scope.toModel();
            
            // Check for items stored in model, root has to be there already
            expect(model.length).toBeGreaterThan(0);
            
            var keys = _.keys(model[0]);
            
            // Check model data
            expect(keys).toContain('id');
            expect(keys).toContain('parentId');
            expect(keys).toContain('name');
            expect(keys).toContain('left');
            expect(keys).toContain('right');
            expect(keys).toContain('collapsed');
            expect(keys).toContain('collapseChildren');
            
            // first node id has to be 0
            expect(model[0].id).toBe(0);
            
            // Parent id of the root has to be null
            expect(model[0].parentId).toBe(null);
            
            // Default root node name has to be 'Root'
            expect(model[0].name).toBe('Root');
            
            // For single node left and right has to be 1 and 2
            expect(model[0].left).toBe(1);
            expect(model[0].right).toBe(2);
            
            // Just created node has not to be collapsed
            expect(model[0].collapsed).toBe(false);
            expect(model[0].collapseChildren).toBe(false);
            
        });
        
        // Test if data is correctly loaded from object model
        it(controllerName + ' :: fromModel', function(){
            
            // Prepare a model
            var model = [
                {
                    id: 0,
                    parentId: null, 
                    name: "Root",
                    left: 1,
                    right: 4,
                    collapsed: false,
                    collapseChildren: false
                },
                {
                    id: 1,
                    parentId: 0, 
                    name: "2-3",
                    left: 2,
                    right: 3,
                    collapsed: false,
                    collapseChildren: false
                }
            ];
            
            // Load data
            scope.fromModel(model);
            
            // Should have two nodes
            expect(scope.nodes.length).toBe(2);
            
            var node = scope.nodes[1];
            
            // check node properties
            expect(node.id).toBe(1);
            expect(node.parentId).toBe(0);
            expect(node.name).toBe('2-3');
            expect(node.left).toBe(2);
            expect(node.right).toBe(3);
            expect(node.collapsed).toBe(false);
            expect(node.collapseChildren).toBe(false);
        });
        
    });

  });
});
