/**
 * Iterative Tree controller
 * Angular controller, to handle and manage tree in an iterative manner.
 * Current tree approach is a JS realization of Nested Sets method.
 * 
 * More information about the method could be found as follows:
 *      https://en.wikipedia.org/wiki/Nested_set_model
 *      http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
 *      
 * Iterative tree nodes are instances of IterativeTreeNode type.
 * Controller handles saving and loading data to localStorage under 'iterativeStorageKey' defined in values/tree-values.js
 */

'use strict';

angular.module("treeModule")
.controller("IterativeTree", ["$scope", "IterativeTreeNode", "iterativeStorageKey", function($scope, IterativeTreeNode, iterativeStorageKey) {
        
        /**
         * Tree initialization
         * @returns {undefined}
         */
        $scope.init = function(name) {
            /**
             * Nodes storage (we have plain structure in NestedSets)
             */
            $scope.nodes = [];
            
            /**
             * Counter, used for basic unique id generation for nodes
             * @default
             */
            $scope.maxid = 0;
            
            /**
             * Root iterative tree node
             * @default
             */
            $scope._addNode(new IterativeTreeNode({id: 0, name: name || "Root"}));
        };
        
        /**
         * Simple unique id generation, used as tree nodes identifier
         * @returns {Number}
         */
        $scope._uniqId = function() {
            return ++($scope.maxid);
        };
        
        /**
         * Indicates whether left or right parameters is out of bounds of the tree.
         * @param {int} left
         * @param {int} right
         * @returns {Boolean}
         */
        $scope._outOfBounds = function(left, right) {
            return (left < 1 || right > $scope.nodes.length * 2);
        };
        
        /**
         * Adds node to a nested set tree, saves the tree state.
         * @param {IterativeTreeNode} node - Node to add
         * @param {int} left - insertion point 'left' indicator
         * @param {int} right - insertion point 'right' indicator
         * @returns {undefined}
         */
        $scope._addNode = function(node, left, right) {
            
            // In case we have no nodes, just add a singne node with
            // indicators 1 and 2
            if ($scope.nodes.length == 0) {
                _.extend(node, {left: 1, right: 2});
                $scope.nodes.push(node);
                return;
            }
            
            // According to nested sets method, 'left' cannot be less than 1
            if (left < 1) {
                left = 1;
            }
            
            // Same to the 'right', it cannot be greater than double number of nodes
            if (right > $scope.nodes.length * 2) {
                right = $scope.nodes.length * 2;
            }
            
            // Find the connecting node
            var connectingNode = $scope.getNode(left, right);
            if (connectingNode === undefined) {
                // console.warn("Invalid connection point: ", left, right);
                return;
            }
            
            // Update other nodes parameters
            _.each($scope.nodes, function(n) {
                if (n.right > left) { n.right += 2; }
                if (n.left > left) { n.left += 2; }
            });
            
            // Update current node parameters also
            node.left = left + 1;
            node.right = left + 2;
            
            // Add new node record into our tree nodes array
            $scope.nodes.push(node);
            
            // Save tree
            $scope.save();
        };
        
        /**
         * Finds the node with 'left' and 'right' parameters.
         * @param {int} left
         * @param {int} right
         * @returns {IterativeTreeNode|undefined}
         */
        $scope.getNode = function(left, right) {
            var result = null;
            
            // Check whether we are out of bounds of a tree
            if ($scope._outOfBounds(left, right)) {
                return;
            }

            // Search trough stored nodes
            _.each($scope.nodes, function(n) {
                if (n.left === left && n.right === right) {
                    result = n;
                    return;
                }
            });

            // Found the node
            if (result) {
                return result;
            }

            // connectors can only diff by 1
            if (right - left === 1) {
                return null;
            }  // node connector (not supported)

            // or nothing
        };
        
        /**
         * Adds a new node to provided 'data' node
         * @param {IterativeTreeNode} data
         * @returns {undefined}
         */
        $scope.add = function(data) {
            var id = $scope._uniqId();
            $scope._addNode(new IterativeTreeNode({id: id, name: data.id + '-' + id, parentId: data.id, collapsed: data.collapseChildren}), data.left, data.right);
        };
        
        /**
         * Removes a node from the tree, saves the tree state.
         * @param {IterativeTreeNode} node
         * @returns {undefined}
         */
        $scope.remove = function(node) {
            var left = node.left,
                    right = node.right,
                    width = node.right - node.left + 1,
                    rname = null;
            
            // Remove node and all it's subnodes
            _.each($scope.nodes, function(n) {
                if (n.left >= left && n.right <= right) {
                    // Keep root node name, as we always should have one
                    if ($scope.isRoot(n)) {
                        rname = n.name;
                    }
                    $scope.nodes = _.without($scope.nodes, n);
                }
            });

            // Update nodes parameters
            _.each($scope.nodes, function(n) {
                if (n.right > right) {
                    n.right = n.right - width;
                }
                if (n.left > right) {
                    n.left = n.left - width;
                }
            });

            // If all nodes removed, create new root node
            if ($scope.nodes.length < 1) {
                $scope.init(rname);
            }
            
            // Save tree
            $scope.save();
        };
        
        /**
         * Calculates provided node depth within a tree
         * @param {IterativeTreeNode} node
         * @returns {Number} - depth
         */
        $scope.depth = function(node) {
            var depth = -1;

            _.each($scope.nodes, function(p) {
                if (node.left >= p.left && node.left <= p.right) {
                    depth++;
                }
            });

            return depth < 0 ? 0 : depth;
        };
        
        /**
         * Converts node object and it's children into a serializable model for further saving
         * @returns {Array}
         */
        $scope.toModel = function() {
            var model = [];
            _.each($scope.nodes, function(node) {
                model.push(node.toModel());
            });
            return model;
        };
        
        /**
         * Restores node instance properties from object model
         * @param {Array} model
         * @returns {undefined}
         */
        $scope.fromModel = function(model) {
            if (!model) {
                return;
            }
            $scope.nodes = [];
            _.each(model, function(m) {
                $scope.nodes.push(new IterativeTreeNode(m));
            });
        };
        
        /**
         * Loads tree state from localStorage
         * @returns {undefined}
         */
        $scope.load = function() {
            var data = JSON.parse(localStorage.getItem(iterativeStorageKey));
            if (data) {
                $scope.maxid = data.maxid || 0;
                if (data.data) {
                    $scope.fromModel(data.data);
                }
            }
        };
        
        /**
         * Saves tree state to localStorage
         * @returns {undefined}
         */
        $scope.save = function() {
            localStorage.setItem(iterativeStorageKey, JSON.stringify({maxid: $scope.maxid, data: $scope.toModel()}));
        };
        
        /**
         * Indicates wheher node is a tree root
         * @param {IterativeTreeNode} data
         * @returns {Boolean}
         */
        $scope.isRoot = function(data) {
            return data.id !== undefined && data.id === 0;
        };
        
        /**
         * Resets the tree and saves tree state.
         * @returns {undefined}
         */
        $scope.reset = function() {
            $scope.init();
            $scope.save();
        };
        
        /**
         * Switches tree node into an edit-name mode
         * @param {IterativeTreeNode} data
         * @returns {undefined}
         */
        $scope.editName = function(data) {
            data.editname = data.name;
            data.edit = true;
        };
        
        /**
         * Saves name for a given node, switches edit mode for a node off 
         * @param {IterativeTreeNode} data
         * @returns {undefined}
         */
        $scope.saveName = function(data) {
            if (data.name != data.editname) {
                data.name = data.editname;
                $scope.save();
            }
            data.edit = false;
        };
        
        /**
         * Toggles collapse/expand state of the node (subnodes left untouched),
         * saves tree state.
         * @param {IterativeTreeNode} data
         * @returns {undefined}
         */
        $scope.toggleCollapse = function(data) {
            data.collapseChildren = !data.collapseChildren;
            $scope._collateChildren(data);
            $scope.save();
        };
        
        /**
         * Finds node parent
         * @param {IterativeTreeNode} node
         * @returns {IterativeTreeNode}
         */
        $scope.getParent = function(node) {
            // never used so far
            return _.find($scope.nodes, function(n) {
                return n.id == node.parentId;
            });
        };
        
        /**
         * Collapse child nodes of a node
         * @param {IterativeTreeNode} node
         * @returns {undefined}
         */
        $scope._collateChildren = function(node) {
            _.each($scope.nodes, function(n) {
                if (n.left > node.left && n.right < node.right) {
                    n.collapsed = node.collapseChildren;
                    if (!node.collapseChildren) {
                        n.collapseChildren = false;
                    }
                }
            });
        };
        
        /** init and load tree */
        $scope.init();
        $scope.load();
    }]);
