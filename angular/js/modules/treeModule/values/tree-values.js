/*!
 * Pre-configured values/constants for the project
 */

'use strict';

angular.module("treeModule")
/**
 * Indicates key for localStorage, to store data of recursive tree
 * @constant
 * @type {string}
 * @default
 */
.value("recursiveStorageKey", "rtree")

/**
 * Indicates key for localStorage, to store data of iterative tree
 * @constant
 * @type {string}
 * @default
 */
.value("iterativeStorageKey", "itree");
