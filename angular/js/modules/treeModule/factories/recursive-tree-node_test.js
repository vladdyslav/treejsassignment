/*!
 * Recursive tree node tests
 */

'use strict';

describe('treeModule RecursiveTreeNode tests', function() {

    var factoryName = "RecursiveTreeNode";

    beforeEach(module('treeModule'));

    var $RecursiveTreeNode;
    
    /**
     * Init every test in the scope with following
     */
    beforeEach(inject(function(RecursiveTreeNode) {
        $RecursiveTreeNode = RecursiveTreeNode;
    }));
    
    /**
     * CleanUp
     */
    afterEach(function(){ $RecursiveTreeNode = null; });

    // Test factory incjection
    describe(factoryName + ' Factory Injection', function() {

        it('Injection Check', function() {
            expect($RecursiveTreeNode).toBeDefined();
        });
        
    });

    describe(factoryName + ' Factory Tests', function() {
        
        // Instantiate from predefined model
        var instance;
        
        /**
         * Init every test in the scope with following
         */
        beforeEach(function() {
            var model = {
                id: 0,
                name: "Root",
                collapsed: false,
                nodes: []
            };
            instance = new $RecursiveTreeNode(model);
        });
        
        /**
         * CleanUp
         */
        afterEach(function(){ instance = null; });

        // Check if the instance is correctly created
        it('Create new instance', function() {
            expect(instance.id).toBe(0);
            expect(instance.name).toBe("Root");
            expect(instance.collapsed).toBe(false);
            expect(_.isArray(instance.nodes)).toBe(true);
        });

        // Test instance to model conversion
        it('toModel', function() {
            var model = instance.toModel();

            var keys = _.keys(model);

            expect(keys).toContain('id');
            expect(keys).toContain('name');
            expect(keys).toContain('collapsed');
            expect(keys).toContain('nodes');

            expect(model.id).toBe(0);
            expect(model.name).toBe('Root');
            expect(model.collapsed).toBe(false);
            expect(_.isArray(model.nodes)).toBe(true);

        });

        // Test creating instance from model
        it('fromModel', function() {
            var model = {
                id: 123,
                name: "SomeNode",
                collapsed: true,
                collapseChildren: false,
                nodes: [
                    {
                        id: 124,
                        name: "SomeNodeChild1",
                        collapsed: false,
                        collapseChildren: false,
                        nodes: []
                    },
                    {
                        id: 125,
                        name: "SomeNodeChild2",
                        collapsed: false,
                        collapseChildren: false,
                        nodes: []
                    }
                ]
            };

            instance.fromModel(model);
            
            var i = instance;
            
            expect(i.id).toBe(123);
            expect(i.name).toBe('SomeNode');
            expect(i.collapsed).toBe(true);
            expect(_.isArray(i.nodes)).toBe(true);
            
            i = instance.nodes[0];
            
            expect(i.id).toBe(124);
            expect(i.name).toBe('SomeNodeChild1');
            expect(i.collapsed).toBe(false);
            expect(_.isArray(i.nodes)).toBe(true);
            
            i = instance.nodes[1];
            
            expect(i.id).toBe(125);
            expect(i.name).toBe('SomeNodeChild2');
            expect(i.collapsed).toBe(false);
            expect(_.isArray(i.nodes)).toBe(true);
            
        });

    });
});