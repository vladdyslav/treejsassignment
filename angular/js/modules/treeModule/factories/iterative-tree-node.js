/*!
 * Iterative Tree node factory
 * Describes a tree node object for iterative tree.
 */

'use strict';

angular.module("treeModule").factory("IterativeTreeNode", function() {
    function IterativeTreeNode(data) {
        
        // Extending object with required properties
        _.extend(this, {
            id: null,
            parentId: null,
            name: "",
            left: 0,
            right: 0,
            collapsed: false,
            collapseChildren: false,
            edit: false
        });
        
        // Extending object with required methods
        _.extend(this, {
            /**
             * Indicates if node has children
             * @returns {Boolean}
             */
            hasChildren: function() {
                return this.right - this.left > 1;
            },
            
            /**
             * Converts node object and it's children into a serializable model for further saving
             * @returns {model object}
             */
            toModel: function() {
                var model = {
                    id: this.id,
                    parentId: this.parentId,
                    name: this.name,
                    left: this.left,
                    right: this.right,
                    collapsed: this.collapsed,
                    collapseChildren: this.collapseChildren
                };
                return model;
            },
            
            /**
             * Restores node instance properties from object model
             * @param {object} model
             * @returns {undefined}
             */
            fromModel: function(model) {
                if (!model) { return; }
                this.id = model.id;
                this.parentId = (model.parentId === undefined ? null : model.parentId);
                this.name = model.name;
                this.left = model.left;
                this.right = model.right;
                this.collapsed = model.collapsed || false;
                this.collapseChildren = model.collapseChildren || false;
            }
        });
        
        // Set properties provided in data on object instantiation
        this.fromModel(data);
    };
    return (IterativeTreeNode);
});
