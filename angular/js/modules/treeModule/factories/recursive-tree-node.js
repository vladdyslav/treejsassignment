/*!
 * Recursive Tree node factory
 * Describes a tree node object for recursive tree.
 */

'use strict';

angular.module("treeModule").factory("RecursiveTreeNode", function() {
    function RecursiveTreeNode(data) {
        
        // Extending object with required properties
        _.extend(this, {
            id: null,
            name: "",
            nodes: [],
            collapsed: false,
            edit: false
        });
        
        // Extending object with required methods
        _.extend(this, {
            /**
             * Converts node object and it's children into a serializable model for further saving
             * @returns {model object}
             */
            toModel: function() {
                var model = {
                    id: this.id,
                    name: this.name,
                    nodes: [],
                    collapsed: this.collapsed
                };
                _.each(this.nodes, function(node) {
                    model.nodes.push(node.toModel());
                });
                return model;
            },
            
            /**
             * Restores node instance properties from object model
             * @param {object} model
             * @returns {undefined}
             */
            fromModel: function(model) {
                if (!model) { return; }
                this.id = model.id;
                this.name = model.name;
                this.collapsed = model.collapsed || false;
                
                _.each(model.nodes, function(node) {
                    this.nodes.push(new RecursiveTreeNode(node));
                }, this);
            }
        });
        
        // Set properties provided in data on object instantiation
        this.fromModel(data);
    };
    return (RecursiveTreeNode);
});
