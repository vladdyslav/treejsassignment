/*!
 * Iterative tree node tests
 */

'use strict';

describe('treeModule IterativeTreeNode tests', function() {

    var factoryName = "IterativeTreeNode";

    /**
     * Init every test in the scope with following
     */
    beforeEach(module('treeModule'));

    var $IterativeTreeNode;
    beforeEach(inject(function(IterativeTreeNode) {
        $IterativeTreeNode = IterativeTreeNode;
    }));
    
    /**
     * CleanUp
     */
    afterEach(function(){
        $IterativeTreeNode = null;
    });
    
    // Test factory incjection
    describe(factoryName + ' Factory Injection', function() {

        it('Injection Check', function() {
            expect($IterativeTreeNode).toBeDefined();
        });
        
    });

    describe(factoryName + ' Factory Tests', function() {
        // Instantiate from predefined model
        var instance;
        
        /**
         * Init every test in the scope with following
         */
        beforeEach(function() {
            var model = {
                id: 0,
                parentId: null,
                left: 1,
                right: 2,
                name: "Root",
                collapsed: false,
                collapseChildren: false
            };
            instance = new $IterativeTreeNode(model);
        });
        
        /**
         * CleanUp
         */
        afterEach(function(){
            instance = null;
        });
        
        // Check if the instance is correctly created
        it('Create new instance', function() {
            expect(instance.id).toBe(0);
            expect(instance.parentId).toBe(null);
            expect(instance.left).toBe(1);
            expect(instance.right).toBe(2);
            expect(instance.collapsed).toBe(false);
            expect(instance.collapseChildren).toBe(false);
        });

        // Instance should not have children
        it('HasChildren', function() {
            expect(instance.hasChildren()).toBe(false);
        });

        // Test instance to model conversion
        it('toModel', function() {
            var model = instance.toModel();

            var keys = _.keys(model);

            expect(keys).toContain('id');
            expect(keys).toContain('parentId');
            expect(keys).toContain('name');
            expect(keys).toContain('left');
            expect(keys).toContain('right');
            expect(keys).toContain('collapsed');
            expect(keys).toContain('collapseChildren');

            expect(model.id).toBe(0);
            expect(model.parentId).toBe(null);
            expect(model.name).toBe('Root');
            expect(model.left).toBe(1);
            expect(model.right).toBe(2);
            expect(model.collapsed).toBe(false);
            expect(model.collapseChildren).toBe(false);

        });

        // Test creating instance from model
        it('fromModel', function() {
            var model = {
                id: 123,
                parentId: 4,
                name: "SomeNode",
                left: 13,
                right: 14,
                collapsed: false,
                collapseChildren: false
            };

            instance.fromModel(model);

            expect(instance.id).toBe(123);
            expect(instance.parentId).toBe(4);
            expect(instance.name).toBe('SomeNode');
            expect(instance.left).toBe(13);
            expect(instance.right).toBe(14);
            expect(instance.collapsed).toBe(false);
            expect(instance.collapseChildren).toBe(false);
        });

    });
});