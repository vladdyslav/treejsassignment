# README #

* JS Assignment (Angular Approach)

### About ###
Current project is an **Angular** approach solving assignment.

Current project implements trees using both Recursive and Iterative methods.
Iterative method is using **NestedSets** to demonstrate implementation.

More about NestedSets could be read here:

* https://en.wikipedia.org/wiki/Nested_set_model
* http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/

### How do I get set up? ###

* Prerequisites
    * You have 'nodejs' to be installed

* How to run tests
    * You can do tests by running  **'npm run test-single-run'** from **current** folder.

* Deployment instructions
    * run **'npm install'**
    * start the application **'npm start'**
    * point your browser to **'localhost:8000'**

### Feedback / Contacts ###
* You can contact me on **vladdyslav-at-gmail-dot-com**